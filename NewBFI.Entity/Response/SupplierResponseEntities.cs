﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewBFI.Entity.Response
{
    public class SupplierResponseEntities
    {
        public string SupplierId { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
    }
}
