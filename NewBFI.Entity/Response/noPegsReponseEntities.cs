﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.Response
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class noPegsReponseEntities
    {
        public string Nopeg { get; set; }
        public bool IsExist { get; set; }
    }
}
