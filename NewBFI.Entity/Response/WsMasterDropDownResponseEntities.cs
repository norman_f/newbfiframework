﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.Response
{
    public class WsMasterDropDownResponseEntities
    {
        public string Value { set; get; }
        public string Text { set; get; }
    }
}
