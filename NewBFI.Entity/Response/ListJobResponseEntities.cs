﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.Response
{
    public class ListJobResponseEntities
    {
        public string value { set; get; }
        public string text { set; get; }
    }
}
