﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.BaseJson
{
    public class BaseJsonResponseErrorEntities
    {
        public string Message { get; set; }
        public string Cause { get; set; }
        public string Code { get; set; }

        public BaseJsonResponseErrorEntities(string message, string cause, string code)
        {
            this.Message = message;
            this.Cause = cause;
            this.Code = code;
        }
    }
}
