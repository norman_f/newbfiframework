﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.BaseJson
{
    public class BaseJsonResponseErrorEntity
    {
        public string Message { get; set; }
        public string Cause { get; set; }
        public string Code { get; set; }

        public BaseJsonResponseErrorEntity(string message, string cause, string code)
        {
            Message = message;
            Cause = cause;
            Code = code;
        }

    }
}
