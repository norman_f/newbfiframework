﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.BaseJson
{
    public class BaseJsonResponseHeaderEntity
    {
        public IList<BaseJsonResponseErrorEntity> Errors = new List<BaseJsonResponseErrorEntity>();
    }
}
