﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.BaseJson
{
    public class BaseJsonResponseEntity<T>
    {
        public BaseJsonResponseHeaderEntity Header { get; set; }
        public T Data { get; set; }

        public BaseJsonResponseEntity()
        {
            Header = new BaseJsonResponseHeaderEntity();
        }
    }
}
