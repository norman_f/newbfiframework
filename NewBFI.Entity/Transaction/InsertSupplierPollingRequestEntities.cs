﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.Transaction
{
    public class InsertSupplierPollingRequestEntities
    {
        public string ChangedData { get; set; }
        public string Exchange { get; set; }
        public string RoutingKey { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
