﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewBFI.Entity
{
    public class InsertSupplierRequestEntities
    {
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
    }
}
