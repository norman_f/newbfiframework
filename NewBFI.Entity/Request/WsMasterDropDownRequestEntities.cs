﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.Request
{
    public class WsMasterDropDownRequestEntities
    {
        public string ID { set; get; }
        public string UserID { set; get; }
        public string Password { set; get; }
        public string Data { set; get; }
    }
}
