﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Entity.Request
{
    public class AddSupplierRequestTestEntity
    {
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public List<string> noPegs { get; set; }
    }
}
