﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewBFI.View.Response
{
    public class SupplierResponse
    {
        public string SupplierId { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
    }
}
