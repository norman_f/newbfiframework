﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewBFI.Transaction.Request
{
    public class InsertSupplierRequest
    {
        public string CompanyName { get; set; }
        public string ContactName { get; set; }

    }
}
