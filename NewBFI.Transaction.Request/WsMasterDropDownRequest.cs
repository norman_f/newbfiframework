﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Transaction.Request
{
    public class WsMasterDropDownRequest
    {
        public string ID { set; get; }
        public string UserID { set; get; }
        public string Password { set; get; }
        public string Data { set; get; }
    }
}
