﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Transaction.Request
{
    public class InsertReportRequest
    {
        public string process_id { get; set; }
        public string parameter_sp { get; set; }
        public string parameter_report { get; set; }
        public string usr_upd { get; set; }
        public string report_id { get; set; }
        public string ip_address { get; set; }
        public string report_url { get; set; }
        public bool is_night { get; set; }
    }
}
