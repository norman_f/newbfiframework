﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Transaction.Request
{
    public class AddSupplierTestRequest
    {
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public List<string> noPegs { get; set; }
    }
}
