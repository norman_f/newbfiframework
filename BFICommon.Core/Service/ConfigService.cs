﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace BFICommon.Core.Service
{
    public class ConfigService
    {
        public string GetConnection(string key)
        {
            var basePath = Directory.GetCurrentDirectory();
            var AppSetting = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .Build();
            return AppSetting.GetConnectionString(key);
        }
        public IConfiguration GetAPIUrl(string key)
        {
            var basePath = Directory.GetCurrentDirectory();
            var AppSetting = new ConfigurationBuilder()
               .SetBasePath(basePath)
               .AddJsonFile("appsettings.json")
               .Build();
            return AppSetting.GetSection(key);
        }
    }
}
