﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFICommon.Core
{
    public class ServiceConfiguration
    {
        public string DBConnection { get; set; }

        public string LegacyAPI { get; set; }
    }
}
