﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BFICommon.Core
{
    public class BaseJsonResponse
    {
        public BaseJsonResponseHeader Header { set; get; }
        public object Data { set; get; }

        public BaseJsonResponse()
        {
            this.Header = new BaseJsonResponseHeader();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
