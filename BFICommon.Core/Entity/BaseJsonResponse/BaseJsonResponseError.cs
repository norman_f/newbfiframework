﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFICommon.Core
{
    public class BaseJsonResponseError
    {
        
            public string Message { get; set; }
            public string Cause { get; set; }
            public string Code { get; set; }

            public BaseJsonResponseError(string message, string cause, string code)
            {
                this.Message = message;
                this.Cause = cause;
                this.Code = code;
            }
    }
}
