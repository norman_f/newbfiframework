﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFICommon.Core
{
    public class BaseJsonResponseHeader
    {
        public IList<BaseJsonResponseError> Errors = new List<BaseJsonResponseError>();
    }
}
