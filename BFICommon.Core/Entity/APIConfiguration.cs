﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFICommon.Core.Entity
{
    public class APIConfiguration
    {
        public string ApiBase { get; set; }
        public string ApiKey { get; set; }
        public string ApiValue { get; set; }
        public string Login { get; set; }
    }
}
