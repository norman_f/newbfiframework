﻿using NewBFI.Entity.Response;
using NewBFI.Entity.Request;
using System;
using System.Collections.Generic;
using System.Text;
using NewBFI.Transaction.Action;

namespace NewBFI.Transaction.Controller
{
    public class DropdownTransactionController : BaseTransactionController
    {
        DropdownTransactionAction dropdownTransactionAction;

        public DropdownTransactionController() {
            dropdownTransactionAction = new DropdownTransactionAction(database);
        }
        public WsMasterDropDownResponseEntities AddData(WsMasterDropDownRequestEntities data) {
            // db connect blm digunakan
            //database.Connect();
            var result = dropdownTransactionAction.AddData(data);
            return result;
            //try
            //{
                
            //}
            //catch (Exception ex)
            //{
            //    //database.Rollback();
            //    //throw ex;
            //}
            //finally
            //{
            //    //database.Close();
            //    //database.Dispose();
            //}
        }
    }
}
