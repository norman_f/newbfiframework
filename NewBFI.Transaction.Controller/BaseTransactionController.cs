﻿using System;
using System.Collections.Generic;
using System.Text;
using BFICommon.Dataaccess;

namespace NewBFI.Transaction.Controller
{
    public class BaseTransactionController
    {
        protected DatabaseService database;

        public BaseTransactionController()
        {
            database = new DatabaseService("DBConnection");
        }
    }
}
