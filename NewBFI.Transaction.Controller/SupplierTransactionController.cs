﻿using NewBFI.Entity;
using System;
using NewBFI.Transaction.Action;
using NewBFI.Entity.Request;

namespace NewBFI.Transaction.Controller
{
    public class SupplierTransactionController : BaseTransactionController
    {
        SupplierTransactionAction supplierTransactionAction;
        public SupplierTransactionController()
        {
            supplierTransactionAction = new SupplierTransactionAction(database);
        }
        public string insertSupplier(InsertSupplierRequestEntities data)
        {
            database.Connect();
            try {
                var result=supplierTransactionAction.CekSupplier(data);
                return result;
            }
            catch(Exception ex)
            {
                database.Rollback();
                throw ex;

            } finally {
                database.Close();
                database.Dispose();
            }
        }
        public string addSupplier(AddSupplierRequestTestEntity insert)
        {
            database.Connect();
            try
            {
                var result = supplierTransactionAction.addSupplier(insert);
                return result;
            }
            catch (Exception ex)
            {
                database.Rollback();
                throw ex;

            }
            finally
            {
                database.Close();
                database.Dispose();
            }
        }

        public void InsertForm(string routing_key, string insertSupplierRequest) {
            try
            {
                database.Connect();
                database.BeginTransaction();
                supplierTransactionAction.InsertForm(routing_key, insertSupplierRequest);
                database.Commit();
            }
            catch (Exception e)
            {
                database.Rollback();
                throw e;
            }
            finally
            {
                database.Close();
                database.Dispose();
            }
        }
    }
}
