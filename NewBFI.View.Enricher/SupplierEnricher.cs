﻿using BFICommon.Dataaccess;
using BFICommon.Util;
using NewBFI.API.Proxy;
using NewBFI.Entity.Request;
using NewBFI.Entity.Response;
using NewBFI.Repository.Query;
using System.Collections.Generic;

namespace NewBFI.View.Enricher
{
    public class SupplierEnricher
    {
        SupplierProxy supplierProxy;
        SupplierQuery supplierQueryRepository;
        public SupplierEnricher(DatabaseService database) {
            supplierProxy = new SupplierProxy();
            supplierQueryRepository = new SupplierQuery(database);
        }

        public List<ListJobResponseEntities> getListJob() {
            var data = supplierProxy.getListJob();
            return data;
        }

        public SupplierDetailResponseEntities ShowSupplierDetail(SupplierDetailRequestEntities requestData)
        {
            var data = supplierQueryRepository.ShowSupplierDetail(requestData).Tables[0];
            return data.DataTableToObject<SupplierDetailResponseEntities>();
        }

        // Get List All Supplier
        public List<SupplierDetailResponseEntities> ShowAllSupplier1()
        {
            var data = supplierQueryRepository.ShowAllSupplier().Tables[0];
            return data.DataTableToListObject<SupplierDetailResponseEntities>();
        }
        // Get List All Supplier where Contact title = Marketing manager
        public List<SupplierDetailResponseEntities> ShowAllSupplier()
        {
            var data = supplierQueryRepository.ShowAllSupplier().Tables[0];
            var adata = data.DataTableToListObject<SupplierDetailResponseEntities>();
            List<SupplierDetailResponseEntities> hasil = new List<SupplierDetailResponseEntities>();
            foreach (var item in adata)
            {
                if (item.ContactTitle == "Marketing Manager")
                {
                    SupplierDetailResponseEntities temp_data = new SupplierDetailResponseEntities();
                    temp_data.SupplierID = item.SupplierID;
                    temp_data.CompanyName = item.CompanyName;
                    temp_data.ContactName = item.ContactName;
                    temp_data.ContactTitle = item.ContactTitle;
                    temp_data.Address = item.Address;
                    temp_data.City = item.City;
                    temp_data.Region = item.Region;
                    temp_data.PostalCode = item.PostalCode;
                    temp_data.Country = item.Country;
                    temp_data.Phone = item.Phone;
                    temp_data.Fax = item.Fax;
                    hasil.Add(temp_data);
                }
            }
            return hasil;
        }
    }
}
