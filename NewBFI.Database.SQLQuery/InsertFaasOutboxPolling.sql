﻿INSERT INTO [BISDB].[faas].[outbox_polling]
           ([changed_data]
           ,[exchange]
           ,[routing_key]
           ,[created_date])
     VALUES
           (@ChangedData
           ,@Exchange
           ,@RoutingKey
           ,@CreatedDate)
		   