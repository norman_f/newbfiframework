﻿using System.Data;
using NewBFI.Entity;
using NewBFI.Entity.Response;
using BFICommon.Util;
using NewBFI.Database.SQLQuery.Resource;
using BFICommon.Dataaccess;
using NewBFI.Entity.Request;

namespace NewBFI.Repository.Query
{
    public class SupplierQuery
    {
        DatabaseService database;

        public SupplierQuery(DatabaseService database) {
            this.database = database;
        }
        public DataSet CekSupplierName(InsertSupplierRequestEntities insertSupplierRequestEntities)
        {
            return database.ExecDataSet(DatabaseUtil.ReadSQLQueriesFromResourceFile("QueryCheckSupplier", typeof(SqlQueries)), DatabaseUtil.ObjectToParamSP(insertSupplierRequestEntities));
        }

        public SupplierResponseEntities getSupplierID(SupplierRequestEntities supplierRequestEntities) {
            using (database)
            {
                DataSet dataSet = database.ExecDataSet(DatabaseUtil.ReadSQLQueriesFromResourceFile("QuerySupplierId", typeof(SqlQueries)),
                    DatabaseUtil.ObjectToParamSP(supplierRequestEntities));

                DataTable dt = dataSet.Tables[0];

                var result = dt.DataTableToObject<SupplierResponseEntities>();
                return result;
            }
        }
        public DataSet ShowSupplierDetail(SupplierDetailRequestEntities data)
        {
            return database.ExecDataSet(DatabaseUtil.ReadSQLQueriesFromResourceFile("QuerySupplierId", typeof(SqlQueries)), DatabaseUtil.ObjectToParamSP(data));
        }

        public DataSet ShowAllSupplier()
        {
            return database.ExecDataSet(DatabaseUtil.ReadSQLQueriesFromResourceFile("QueryAllSupplier", typeof(SqlQueries)));
        }
    }
}
