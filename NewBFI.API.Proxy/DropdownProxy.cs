﻿using Microsoft.Extensions.Configuration;
using NewBFI.Entity.Request;
using NewBFI.Entity.Response;
using Newtonsoft.Json;
using System.Xml;
using RestSharp;
using System.Collections.Generic;

namespace NewBFI.API.Proxy
{
    public class DropdownProxy : ProxyBaseUrl
    {
        private const string WSGeneralAPI = "WSGeneralAPI";
        public IConfiguration BaseUrlWSGeneral;
        readonly AppConfig appConfig = new AppConfig();

        public DropdownProxy() {
            BaseUrlWSGeneral = ApiBaseUrl(WSGeneralAPI);
        }

        public WsMasterDropDownResponseEntities AddData(WsMasterDropDownRequestEntities data) {
            string content = JsonConvert.SerializeObject(data);

            XmlDocument doc = JsonConvert.DeserializeXmlNode(content, "WsMasterDropDown");

            string docxml = @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                            <soap:Body>" + doc.InnerXml.ToString().Replace("<WsMasterDropDown>", "<WsMasterDropDown xmlns='http://tempuri.org/'>") + "</soap:Body></soap:Envelope>";

            var client = new RestClient(appConfig.ApiBaseSyariah + "syariah/wsgeneral.asmx?op=WsMasterDropDown&=");
            var request = new RestRequest(Method.POST);
            request.AddHeader("apikey", appConfig.ApiKeySyariah);
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("undefined", docxml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            XmlDocument xDoc = new XmlDocument();

            xDoc.LoadXml(response.Content);

            var datat = JsonConvert.DeserializeObject<List<WsMasterDropDownResponseEntities>>(xDoc.InnerText);

            return datat[0];
        }
    }
}
