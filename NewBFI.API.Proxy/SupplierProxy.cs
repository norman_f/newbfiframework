﻿using Microsoft.Extensions.Configuration;
using NewBFI.Entity.Response;
using NewBFI.Entity.Request;
using BFICommon.Core.Entity;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;
using NewBFI.Entity.BaseJson;

namespace NewBFI.API.Proxy
{
    public class SupplierProxy : ProxyBaseUrl
    {
        private const string LegacyApi = "LegacyAPI";
        public IConfiguration BaseUrlLegacy;

        public SupplierProxy()
        {
            BaseUrlLegacy = ApiBaseUrl(LegacyApi);
        }
        public List<noPegsReponseEntities> checkNoPegs(CheckNoPegsRequestEntities nomor)
        {
            var json = JsonConvert.SerializeObject(nomor);
            APIConfiguration apiConfiguration = BaseUrlLegacy.Get<APIConfiguration>();
            var client = new RestClient(apiConfiguration.ApiBase + "api/v1/employee/check_nopeg");
            var request = new RestRequest(Method.POST);
            request.AddHeader(apiConfiguration.ApiKey, apiConfiguration.ApiValue);
            request.AddHeader("Accept", "application/json");
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = client.Execute(request);
            var data = JsonConvert.DeserializeObject<BaseJsonResponseEntity<List<noPegsReponseEntities>>>(response.Content);

            return data.Data;
        }

        public List<ListJobResponseEntities> getListJob() {
            var apiConfig = BaseUrlLegacy.Get<APIConfiguration>();
            var client = new RestClient(apiConfig.ApiBase);
            var request = new RestRequest("api/v1/employee/dropdown_employee_position", Method.GET);
            request.AddHeader(apiConfig.ApiKey, apiConfig.ApiValue);
            var response = client.Execute(request);
            var data = JsonConvert.DeserializeObject<BaseJsonResponseEntity<List<ListJobResponseEntities>>>(response.Content);

            return data.Data;
        }
    }
}
