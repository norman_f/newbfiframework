﻿using System;
using System.Collections.Generic;
using System.Text;
using BFICommon.Core;
using Microsoft.Extensions.Configuration;

namespace NewBFI.API.Proxy
{
    public abstract class ProxyBaseUrl
    {
        protected IConfiguration BaseApiUrl;

        protected const string LegacyAPI = "LegacyAPI";
        protected const string EmailAPI = "EmailSMSAPI";
        public IConfiguration ApiBaseUrl(string ApiName)
        {
            ConfigService config = new ConfigService();
            BaseApiUrl = config.GetAPIUrl(ApiName);
            return BaseApiUrl;
        }
    }
}
