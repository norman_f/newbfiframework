﻿using Microsoft.Extensions.Configuration;
using System.IO;
using BFICommon.Core;

namespace NewBFI.API.Proxy
{
    public class AppConfig
    {
        private const string UrL = "appsettings.json";
        public static IConfiguration Configuration { get; set; }

        public string ApiBaseSyariah;
        public string ApiKeySyariah;


        public AppConfig()
        {
            var directory = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile(UrL);
            Configuration = builder.Build();

            ApiBaseSyariah = Configuration["ApiBaseSyariah"];
            ApiKeySyariah = Configuration["ApiKeySyariah"];
        }


        public static ServiceConfiguration url
        {
            get
            {
                var directory = Directory.GetCurrentDirectory();
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(UrL);
                Configuration = builder.Build();

                ServiceConfiguration service = new ServiceConfiguration();
                service.LegacyAPI = Configuration["LegacyAPI"];

                return service;
            }
        }
    }
}
