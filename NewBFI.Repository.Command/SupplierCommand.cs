﻿using System.Data;
using NewBFI.Entity;
using BFICommon.Util;
using BFICommon.Dataaccess;
using NewBFI.Entity.Response;
using NewBFI.Database.SQLQuery.Resource;
using System;
using NewBFI.Entity.Transaction;

namespace NewBFI.Repository.Command
{
    public class SupplierCommand
    {
        DatabaseService database;
        public SupplierCommand(DatabaseService database)
        {
            this.database = database;
        }

        public void insertSupplier(InsertSupplierRequestEntities insertSupplierRequestEntities) {
            database.ExecNonQuery(DatabaseUtil.ReadSQLQueriesFromResourceFile("QueryAddSupplier", typeof(SqlQueries)), DatabaseUtil.ObjectToParamSP(insertSupplierRequestEntities)).ToString();
        }

        public void InsertSupplierPolling(InsertSupplierPollingRequestEntities insertSupplierPolling) {
            database.ExecNonQuery(DatabaseUtil.ReadSQLQueriesFromResourceFile("InsertFaasOutboxPolling", typeof(SqlQueries)), DatabaseUtil.ObjectToParamSP(insertSupplierPolling));
        }
    }
}
