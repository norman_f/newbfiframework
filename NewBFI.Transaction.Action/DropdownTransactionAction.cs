﻿using BFICommon.Dataaccess;
using NewBFI.API.Proxy;
using NewBFI.Entity.Request;
using NewBFI.Entity.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.Transaction.Action
{
    public class DropdownTransactionAction
    {
        readonly DropdownProxy dropdownProxy;
        public DropdownTransactionAction(DatabaseService database) {
            dropdownProxy = new DropdownProxy();
        }

        public WsMasterDropDownResponseEntities AddData(WsMasterDropDownRequestEntities data) {
            return dropdownProxy.AddData(data);
        }
    }
}
