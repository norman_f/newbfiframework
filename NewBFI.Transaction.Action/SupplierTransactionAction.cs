﻿using NewBFI.Repository.Command;
using NewBFI.Repository.Query;
using BFICommon.Dataaccess;
using NewBFI.Entity.Response;
using NewBFI.Entity;
using BFICommon.Util;
using NewBFI.Entity.Request;
using NewBFI.API.Proxy;
using System;
using NewBFI.Entity.Transaction;
using Newtonsoft.Json;

namespace NewBFI.Transaction.Action
{
    public class SupplierTransactionAction
    {
        SupplierCommand supplierCommandRepository;
        SupplierQuery supplierQueryRepository;
        readonly SupplierProxy supplierProxy;

        public SupplierTransactionAction(DatabaseService database) {
            supplierCommandRepository = new SupplierCommand(database);
            supplierQueryRepository = new SupplierQuery(database);
            supplierProxy = new SupplierProxy();
        }

        public string CekSupplier(InsertSupplierRequestEntities insertSupplierRequestEntities) {
            var data = supplierQueryRepository.CekSupplierName(insertSupplierRequestEntities).Tables[0];
            var hasildata = data.DataTableToObject<SupplierResponseEntities>();
            if (hasildata == null)
            {
                supplierCommandRepository.insertSupplier(insertSupplierRequestEntities);
                return "Data berhasil ditambahkan";
            }
            else
            {
                return "supplier sudah ada";
            }
        }
        public void insertSupplier(InsertSupplierRequestEntities insertSupplierRequestEntities) {
             supplierCommandRepository.insertSupplier(insertSupplierRequestEntities);
        }
        public string addSupplier(AddSupplierRequestTestEntity insert) {
            var result="";
            CheckNoPegsRequestEntities noPegs = new CheckNoPegsRequestEntities()
            {
                noPegs = insert.noPegs
            };
            var check = supplierProxy.checkNoPegs(noPegs);

            if (check[0].IsExist == true)
            {
                InsertSupplierRequestEntities item = new InsertSupplierRequestEntities()
                {
                    CompanyName = insert.CompanyName,
                    ContactName = insert.ContactName
                };
                insertSupplier(item);
                result = "berhasil mendaftarkan supplier";
            }
            else
            {
                result = "tidak dapat mendaftar";
            }

            return result;
        }

        public void InsertForm(string routing_key, string insertSupplierRequest) {
            InsertSupplierRequestEntities data = JsonConvert.DeserializeObject<InsertSupplierRequestEntities>(insertSupplierRequest);

            switch (routing_key) {
                case "q.salestrax.update-status-lead-faas.work":
                    insertSupplier(data);
                    insertSupplierPolling(insertSupplierRequest);
                    break;
                default: throw new Exception();
            }
        }

        private void insertSupplierPolling(string insertSupplierRequest)
        {
            InsertSupplierPollingRequestEntities insertSupplierPollingRequestEntities = new InsertSupplierPollingRequestEntities
            {
                ChangedData = insertSupplierRequest,
                Exchange = "x.salestrax.work",
                RoutingKey = "update-status-lead-faas",
                CreatedDate = DateTime.Now
            };
            supplierCommandRepository.InsertSupplierPolling(insertSupplierPollingRequestEntities);
        }
    }
}
