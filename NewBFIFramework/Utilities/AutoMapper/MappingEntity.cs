﻿using AutoMapper;
using NewBFI.Entity;
using NewBFI.Entity.Request;
using NewBFI.Entity.Response;
using NewBFI.Transaction.Request;
using NewBFI.View.Request;
using NewBFI.View.Response;

namespace NewBFIFramework.Utilities.AutoMapper
{
    public class MappingEntity : Profile
    {
        public MappingEntity()
        {
            // Create Map <Source, Destination>
            CreateMap<SupplierRequest, SupplierRequestEntities>();
            CreateMap<InsertSupplierRequest, InsertSupplierRequestEntities>();
            CreateMap<SupplierResponseEntities, SupplierResponse>();
            CreateMap<AddSupplierTestRequest, AddSupplierRequestTestEntity>();
            CreateMap<InsertReportRequest, InsertReportRequestEntities>();
            CreateMap<ListJobRequest, ListJobRequestEntities>();
            CreateMap<WsMasterDropDownRequest, WsMasterDropDownRequestEntities>();
            CreateMap<SupplierDetailRequest, SupplierDetailRequestEntities>();
        }
    }
}
