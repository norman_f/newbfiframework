﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewBFIFramework.Utilities.AutoMapper;
using AutoMapper;

namespace NewBFIFramework.Utilities.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingEntity>();
            });
#pragma warning restore CS0618 // Type or member is obsolete
        }
    }
}
