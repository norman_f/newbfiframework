﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NewBFIFramework.Extentions;
using NewBFIFramework.Utilities.AutoMapper;
using NewBFI.RabbitMQ;
using BFICommon.Core;
using NewBFI.Entity;

namespace NewBFIFramework
{
    public class Startup
    {
        IConfiguration RabbitMQ;
        private readonly RabbitMQEntities rabbitMQEntities;
        public Startup(IConfiguration configuration)
        {
            
            ConfigService config = new ConfigService();
            RabbitMQ = config.GetAPIUrl("RabbitMQ");
            rabbitMQEntities = RabbitMQ.Get<RabbitMQEntities>();

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            AutoMapperConfiguration.Configure();
            services.ConfigureSwagger();

            if (rabbitMQEntities.Status == "On")
            {
                services.AddHostedService<ConsumeRabbitMQHostedService>();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("../swagger/v1/swagger.json", "Swagger API Financial Documentation v1");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
