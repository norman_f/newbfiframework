﻿using Microsoft.AspNetCore.Mvc;
using NewBFI.Transaction.Request;
using BFICommon.Core;
using AutoMapper;
using NewBFI.Entity;
using NewBFI.Transaction.Controller;
using NewBFI.Entity.Request;
using NewBFI.View.Controller;
using BFICommon.Util.Logger;
using NewBFI.View.Request;

namespace NewBFIFramework.Controllers
{
    [Route("api")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        SupplierTransactionController supplierTransactionController = new SupplierTransactionController();
        SupplierViewController supplierViewController = new SupplierViewController();
        BaseJsonResponse baseJsonResponse = new BaseJsonResponse();
        CommonLog _logger = new CommonLog();

        // Get Data Supplier by Supplierid
        [Route("supplier")]
        [HttpGet]
        public IActionResult ShowSupplierDetail([FromQuery] SupplierDetailRequest data)
        {
            var parameter = Mapper.Map<SupplierDetailRequest, SupplierDetailRequestEntities>(data);
            var result = supplierViewController.ShowSupplierDetail(parameter);
            baseJsonResponse.Data = result;
            return Ok(baseJsonResponse);
        }

        // Get All data Supplier
        [Route("allsupplier")]
        [HttpGet]
        public IActionResult ShowAllSupplier()
        {
            _logger.logExecutionTime(NLog.LogLevel.Info.Name, "NewBFI", "Show All Supplier", "START");
            var result = supplierViewController.ShowAllSupplier();
            _logger.logExecutionTime(NLog.LogLevel.Info.Name, "NewBFI", "Show All success", "END");
            return Ok(result);
        }

        [Route("addsupplier-with-check-northwinddb")]
        [HttpPost]
        public IActionResult AddSupplier(InsertSupplierRequest insertSupplier)
        {
            _logger.logExecutionTime(NLog.LogLevel.Info.Name, "NewBFI", "Add Supplier", "START");
            var parameter = Mapper.Map<InsertSupplierRequest, InsertSupplierRequestEntities>(insertSupplier);
            var result = supplierTransactionController.insertSupplier(parameter);
            _logger.logExecutionTime(NLog.LogLevel.Info.Name, "NewBFI", "Add supplier success", "END");
            return Ok(result);
        }

        [Route("addsupplier-with-check-nopeg")]
        [HttpPost]
        public IActionResult AddSupplierTest([FromBody]AddSupplierTestRequest supplier) {
            var parameter = Mapper.Map<AddSupplierTestRequest, AddSupplierRequestTestEntity>(supplier);
            var result = supplierTransactionController.addSupplier(parameter);
            return Ok(result);
        }

        [Route("list-job")]
        [HttpGet]
        public IActionResult listJob() {
            var result = supplierViewController.getListJob();
            baseJsonResponse.Data = result;
            return Ok(baseJsonResponse);
        }

        DropdownTransactionController dropdownTransactionController = new DropdownTransactionController();

        [Route("master-dropdown")]
        [HttpPost]
        public IActionResult WsMasterDropdown(WsMasterDropDownRequest data) {
            var parameter = Mapper.Map<WsMasterDropDownRequest, WsMasterDropDownRequestEntities>(data);
            var result = dropdownTransactionController.AddData(parameter);
            return Ok(result);
        }
    }
}