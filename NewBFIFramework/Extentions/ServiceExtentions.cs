﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NewBFIFramework.Extentions
{
    public static class ServiceExtentions
    {
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Title = "Swagger API Financial Documentation",
                    Version = "v1"

                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                foreach (var name in Directory.GetFiles("SwaggerXML", "*.XML", SearchOption.AllDirectories))
                {
                    options.IncludeXmlComments(filePath: name);
                }

                options.EnableAnnotations();
            });


        }
    }
}
