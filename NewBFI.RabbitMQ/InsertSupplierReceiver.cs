﻿using System;
using System.Collections.Generic;
using System.Text;
using BFI.MQ.Library;
using NewBFI.Transaction.Controller;

namespace NewBFI.RabbitMQ
{
    public class InsertSupplierReceiver : BFI_MQ_Customer
    {
        private readonly BFIMQ_Instance bFIMQ_Instance;
        private readonly SupplierTransactionController supplierTransactionController;
        private readonly int MaxRetry = 3;
        public InsertSupplierReceiver(List<BFI_MQ_Endpoints> endpoints, string username, string password) {
            bFIMQ_Instance = new BFIMQ_Instance(endpoints, username, password);
            supplierTransactionController = new SupplierTransactionController();
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered,
            string exchange, string routingKey, IDictionary<string, object> properties, byte[] body)
        {
            int retry_value = 0;
            if (properties != null && properties.Count > 0)
            {
                object value;
                if (properties.TryGetValue("x-death", out value))
                {
                    List<object> xDeathList = (List<object>)properties["x-death"];
                    if (xDeathList != null)
                    {
                        Dictionary<string, object> dict = (System.Collections.Generic.Dictionary<string, object>)xDeathList[0];
                        retry_value = (int)(long)dict["count"] == null ? 0 : (int)(long)dict["count"];
                    }
                }
            }
            try
            {
                if (retry_value < MaxRetry)
                {

                    try
                    {
                        supplierTransactionController.InsertForm(routingKey, Encoding.UTF8.GetString(body));
                        BasicAck(deliveryTag);
                    }
                    catch (Exception e)
                    {
                        BasicReject(deliveryTag, false);
                    }
                }
                else
                {
                    //republish to dead
                    BasicAck(deliveryTag);
                    bFIMQ_Instance.Publish_Message(exchange.Replace("work", "dead"), routingKey, Encoding.UTF8.GetString(body));
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("Error: " + e.Message);
            }
        }

    }
}
