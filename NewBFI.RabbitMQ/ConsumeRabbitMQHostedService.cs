﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using BFI.MQ.Library;

namespace NewBFI.RabbitMQ
{
    public class ConsumeRabbitMQHostedService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly string QUEUE_LEAD_LINE0_UPDATE_TO_DB_WORK = "q.salestrax.update-status-lead-faas.work";
        private string USERNAME;
        private string PASSWORD;

        readonly List<BFI_MQ_Endpoints> endpoints = new List<BFI_MQ_Endpoints>();
        public ConsumeRabbitMQHostedService(ILoggerFactory loggerFactory, IConfiguration configuration)
        {
            this._logger = loggerFactory.CreateLogger<ConsumeRabbitMQHostedService>();
            _configuration = configuration;
            RabbitMQConnections();
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            InsertSupplierReceiver messageReceiver = new InsertSupplierReceiver(endpoints, USERNAME, PASSWORD);
            BFIMQ_Instance bFMIQ_Instance = new BFIMQ_Instance(endpoints, USERNAME, PASSWORD);
            bFMIQ_Instance.Set_QOS(0, 1);
            bFMIQ_Instance.Set_Customer_Receiver(messageReceiver, QUEUE_LEAD_LINE0_UPDATE_TO_DB_WORK);
            return Task.CompletedTask;
        }
        private void RabbitMQConnections()
        {
            string[] ConnectionString = _configuration["RabbitMQ:server"].Split(',');
            foreach (var item in ConnectionString)
            {
                string server = item.Split('@')[0];
                USERNAME = item.Split('@')[1].Split(':')[0];
                PASSWORD = item.Split('@')[1].Split(':')[1];
                endpoints.Add(new BFI_MQ_Endpoints() { Hostname = server.Split(':')[0], Port = int.Parse(server.Split(':')[1]) });
            }
        }
    }
}
