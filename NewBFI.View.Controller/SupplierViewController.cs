﻿using NewBFI.API.Proxy;
using NewBFI.Entity.Response;
using NewBFI.Entity.Request;
using System;
using System.Collections.Generic;
using System.Text;
using NewBFI.View.Enricher;
using BFICommon.Dataaccess;

namespace NewBFI.View.Controller
{
    public class SupplierViewController : BaseViewController
    {
        SupplierEnricher supplierEnricher;
        public SupplierViewController() {
            supplierEnricher = new SupplierEnricher(database);
        }

        public List<ListJobResponseEntities> getListJob() {
            try
            {
                database.Connect();
                return supplierEnricher.getListJob();
            }
            catch (Exception ex)
            {
                database.Rollback();
                throw ex;

            }
            finally
            {
                database.Close();
                database.Dispose();
            }
        }

        public SupplierDetailResponseEntities ShowSupplierDetail(SupplierDetailRequestEntities data)
        {
            try
            {
                database.Connect();
                return supplierEnricher.ShowSupplierDetail(data);
            }
            catch (Exception ex)
            {
                database.Rollback();
                throw ex;
            }
            finally
            {
                database.Close();
                database.Dispose();
            }
        }

        public List<SupplierDetailResponseEntities> ShowAllSupplier()
        {
            try
            {
                database.Connect();
                // tinggal ganti sesuai keperluan
                return supplierEnricher.ShowAllSupplier();
            }
            catch (Exception ex)
            {
                database.Rollback();
                throw ex;
            }
            finally
            {
                database.Close();
                database.Dispose();
            }
        }




    }
}
