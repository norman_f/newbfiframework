﻿using System;
using System.Collections.Generic;
using System.Text;
using BFICommon.Dataaccess;

namespace NewBFI.View.Controller
{
    public class BaseViewController
    {
        protected DatabaseService database;
        public BaseViewController() {
            database = new DatabaseService("DBConnection");
        }
    }
}
