﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewBFI.View.Request
{
    public class SupplierDetailRequest
    {
        public string SupplierID { set; get; }
    }
}
